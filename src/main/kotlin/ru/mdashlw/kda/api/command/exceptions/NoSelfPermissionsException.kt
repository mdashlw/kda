package ru.mdashlw.kda.api.command.exceptions

class NoSelfPermissionsException : RuntimeException()
