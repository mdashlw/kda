package ru.mdashlw.kda.api.command

import java.util.*

interface GuildSettings {
    val prefix: String
    val locale: Locale
}
