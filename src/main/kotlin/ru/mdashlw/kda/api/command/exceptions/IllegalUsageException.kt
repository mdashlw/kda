package ru.mdashlw.kda.api.command.exceptions

class IllegalUsageException(message: String) : RuntimeException(message)
