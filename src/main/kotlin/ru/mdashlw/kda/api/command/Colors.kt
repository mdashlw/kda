package ru.mdashlw.kda.api.command

import java.awt.Color

interface Colors {
    var default: Color?
    var success: Color
    var warning: Color
    var error: Color
}
