package ru.mdashlw.kda.api.command.exceptions

class NoMemberPermissionsException : RuntimeException()
