package ru.mdashlw.kda.api.command.annotations

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention
annotation class Text
