package ru.mdashlw.kda.api.command.annotations

@Target(AnnotationTarget.FUNCTION)
@Retention
annotation class CommandFunction
