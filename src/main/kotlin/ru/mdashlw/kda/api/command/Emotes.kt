package ru.mdashlw.kda.api.command

interface Emotes {
    var success: String
    var warning: String
    var error: String
}
