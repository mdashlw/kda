package ru.mdashlw.kda.internal.command.emotes

import ru.mdashlw.kda.api.command.Emotes

object DefaultEmotes : Emotes {
    override var success: String = ""
    override var warning: String = ""
    override var error: String = ""
}
